FROM java:8-jre-alpine

EXPOSE 8080

ADD ./target/bestshop-notification-service.jar /app/

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/bestshop-notification-service.jar"]