package io.gary.bestshop.notification.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EmailNotificationService {

    public void sendCustomerEmail(String username, String title, String message) {
        log.info("\n\n-\t{}\n\n-\tDear {}:\n\n-\t\t {} \n\n-\tThanks & best regards, \n-\tBestShop.inc\n", title, username, message);
    }

    public void sendAdminEmail(String title, String message) {
        log.info("\n\n-\t{}\n\n-\tDear admin:\n\n-\t\t {} \n\n-\tThanks & best regards, \n-\tBestShop.inc\n", title, message);
    }
}
