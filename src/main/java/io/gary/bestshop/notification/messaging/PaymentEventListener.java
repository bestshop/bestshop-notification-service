package io.gary.bestshop.notification.messaging;


import io.gary.bestshop.messaging.event.payment.PaymentReceivedEvent;
import io.gary.bestshop.notification.service.EmailNotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@RequiredArgsConstructor
public class PaymentEventListener {

    private final EmailNotificationService emailNotificationService;

    @StreamListener(MessagingChannels.PAYMENT_RECEIVED_INPUT)
    public void handlePaymentReceivedEvent(PaymentReceivedEvent event) {

        String title = "Payment Received - Order:" + event.getPayment().getOrderId();
        String message = String.format("A payment is just received: %s", event.getPayment());

        emailNotificationService.sendAdminEmail(title, message);
    }
}
