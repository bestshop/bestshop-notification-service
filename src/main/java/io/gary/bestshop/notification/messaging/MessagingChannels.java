package io.gary.bestshop.notification.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface MessagingChannels {

    String ACCOUNT_CREATED_INPUT = "accountCreatedInput";

    @Input(ACCOUNT_CREATED_INPUT)
    SubscribableChannel accountCreatedInput();


    String PRODUCT_CREATED_INPUT = "productCreatedInput";

    @Input(PRODUCT_CREATED_INPUT)
    SubscribableChannel productCreatedInput();


    String PRODUCT_UPDATED_INPUT = "productUpdatedInput";

    @Input(PRODUCT_UPDATED_INPUT)
    SubscribableChannel productUpdatedInput();


    String PRODUCT_REVIEW_ADDED_INPUT = "productReviewAddedInput";

    @Input(PRODUCT_REVIEW_ADDED_INPUT)
    SubscribableChannel productReviewAddedInput();


    String ORDER_CREATED_INPUT = "orderCreatedInput";

    @Input(ORDER_CREATED_INPUT)
    SubscribableChannel orderCreatedInput();


    String ORDER_CANCELLED_INPUT = "orderCancelledInput";

    @Input(ORDER_CANCELLED_INPUT)
    SubscribableChannel orderCancelledInput();


    String ORDER_DELIVERED_INPUT = "orderDeliveredInput";

    @Input(ORDER_DELIVERED_INPUT)
    SubscribableChannel orderDeliveredInput();


    String ORDER_COMPLETED_INPUT = "orderCompletedInput";

    @Input(ORDER_COMPLETED_INPUT)
    SubscribableChannel orderCompletedInput();


    String PAYMENT_RECEIVED_INPUT = "paymentReceivedInput";

    @Input(PAYMENT_RECEIVED_INPUT)
    SubscribableChannel paymentReceivedInput();

}
