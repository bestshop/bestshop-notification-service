package io.gary.bestshop.notification.messaging;

import io.gary.bestshop.messaging.event.product.ProductCreatedEvent;
import io.gary.bestshop.messaging.event.product.ProductReviewAddedEvent;
import io.gary.bestshop.messaging.event.product.ProductUpdatedEvent;
import io.gary.bestshop.notification.service.EmailNotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class ProductEventListener {

    private final EmailNotificationService emailNotificationService;

    @StreamListener(MessagingChannels.PRODUCT_CREATED_INPUT)
    public void handleProductCreatedEvent(ProductCreatedEvent event) {

        String title = "Product Created - id:" + event.getNewProduct().getId();
        String message = String.format("A product is just created: %s", event.getNewProduct());

        emailNotificationService.sendAdminEmail(title, message);
    }

    @StreamListener(MessagingChannels.PRODUCT_UPDATED_INPUT)
    public void handleProductUpdatedEvent(ProductUpdatedEvent event) {

        String title = "Product Updated - id:" + event.getOldProduct().getId();
        String message = String.format("A product is just updated: \noldProduct=%s\nnewProduct=%s", event.getOldProduct(), event.getUpdatedProduct());

        emailNotificationService.sendAdminEmail(title, message);
    }

    @StreamListener(MessagingChannels.PRODUCT_REVIEW_ADDED_INPUT)
    public void handleProductReviewAddedEvent(ProductReviewAddedEvent event) {

        String title = "Product Review Added - id:" + event.getProduct().getId();
        String message = String.format("A product review is just added: %s", event.getNewReview());

        emailNotificationService.sendAdminEmail(title, message);
    }
}
