package io.gary.bestshop.notification.messaging;


import io.gary.bestshop.messaging.event.order.OrderCancelledEvent;
import io.gary.bestshop.messaging.event.order.OrderCompletedEvent;
import io.gary.bestshop.messaging.event.order.OrderCreatedEvent;
import io.gary.bestshop.messaging.event.order.OrderDeliveredEvent;
import io.gary.bestshop.notification.service.EmailNotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@RequiredArgsConstructor
public class OrderEventListener {

    private final EmailNotificationService emailNotificationService;

    @StreamListener(MessagingChannels.ORDER_CREATED_INPUT)
    public void handleOrderCreatedEvent(OrderCreatedEvent event) {

        String title = "Order Created";
        String message = String.format("Your order is created: %s", event.getOrder());

        emailNotificationService.sendCustomerEmail(event.getOrder().getPurchasedBy(), title, message);
    }

    @StreamListener(MessagingChannels.ORDER_DELIVERED_INPUT)
    public void handleOrderDeliveredEvent(OrderDeliveredEvent event) {

        String title = "Order Delivered";
        String message = String.format("Your order is delivered: %s", event.getOrder());

        emailNotificationService.sendCustomerEmail(event.getOrder().getPurchasedBy(), title, message);
    }

    @StreamListener(MessagingChannels.ORDER_CANCELLED_INPUT)
    public void handleOrderCancelledEvent(OrderCancelledEvent event) {

        String title = "Order Cancelled";
        String message = String.format("Your order is cancelled: %s", event.getOrder());

        emailNotificationService.sendCustomerEmail(event.getOrder().getPurchasedBy(), title, message);
    }

    @StreamListener(MessagingChannels.ORDER_COMPLETED_INPUT)
    public void handleOrderCompletedEvent(OrderCompletedEvent event) {

        String title = "Order Completed";
        String message = String.format("Your order is completed: %s", event.getOrder());

        emailNotificationService.sendCustomerEmail(event.getOrder().getPurchasedBy(), title, message);
    }
}
