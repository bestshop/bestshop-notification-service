package io.gary.bestshop.notification.messaging;

import io.gary.bestshop.messaging.dto.AccountDto;
import io.gary.bestshop.messaging.event.account.AccountCreatedEvent;
import io.gary.bestshop.notification.service.EmailNotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class AccountEventListener {

    private final EmailNotificationService emailNotificationService;

    @StreamListener(MessagingChannels.ACCOUNT_CREATED_INPUT)
    public void handleAccountCreatedEvent(AccountCreatedEvent event) {

        sendEmailToUser(event.getNewAccount());

        sendEmailToAdmin(event.getNewAccount());
    }

    private void sendEmailToAdmin(AccountDto newAccount) {

        String title = "Account Created - username:" + newAccount.getUsername();
        String message = String.format("A new account is just created: %s", newAccount);

        emailNotificationService.sendAdminEmail(title, message);
    }

    private void sendEmailToUser(AccountDto newAccount) {

        String title = "Account Created";
        String message = String.format("Your account is just created: %s", newAccount);

        emailNotificationService.sendCustomerEmail(newAccount.getUsername(), title, message);
    }
}
